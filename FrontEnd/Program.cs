﻿using ConsoleApplication4.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontEnd
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("=============================");
            Console.WriteLine("Bienvenue dans Age Of Empires");
            Console.WriteLine("=============================");

            Console.Write("\nVoulez-vous commencer une nouvelle partie ? O/N ");
            char begin = Console.ReadLine().ToCharArray()[0];
            #region Début de la partie (si l'utilisateur tape 'O' pour commencer une partie)
            if (begin == 'O' || begin == 'o')
            {
                ForumFactory forum = ForumFactories.GetFactory("ConsoleApplication4.Player1");
                Console.Write("Entrez votre nom : ");
                string name = Console.ReadLine();

                for (int i = 0; i < 5; i++)
                {
                    Console.WriteLine("\nQuel bâtiment voulez-vous construire ?");
                    Console.Write("Caserne | Grenier | Habitation | Port | puit de Stockage : ");
                    char rep = Console.ReadLine().ToCharArray()[0];

                    switch (rep)
                    {
                        case 'C':
                        case 'c':
                            Caserne caserne = forum.CreateCaserne(name);
                            Console.Write("Voulez-vous créer un fantassin ? O/N ");
                            char fant = Console.ReadLine().ToCharArray()[0];
                            if (fant == 'O' || fant == 'o') { caserne.CreateFantassin(); }
                            else
                            {
                                break;
                            }
                            break;
                        case 'G':
                        case 'g':
                            forum.CreateGrenier(name);
                            break;
                        case 'H':
                        case 'h':
                            forum.CreateHabitation(name);
                            break;
                        case 'P':
                        case 'p':
                            forum.CreatePort(name);
                            break;
                        case 'S':
                        case 's':
                            forum.CreatePuit(name);
                            break;
                        default:
                            Console.WriteLine("Choisissez un bâtiment existant !");
                            break;
                    }
                }

                Console.ReadKey();
            }
            #endregion

            #region L'utilisateur ne souhaite pas commencer une partie
            else
            {
                Console.WriteLine("Au revoir !");
            } 
            #endregion

        }
    }
}
