﻿using ConsoleApplication4.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication4.Player1
{
    class Player1ForumFactory : ForumFactory
    {
        public override Caserne CreateCaserne(string name)
        {
            Console.WriteLine($"\n{name} vient de créer une caserne !");
            Player1Caserne caserne = new Player1Caserne();
            Console.WriteLine($"PV : {caserne.Health}");
            return caserne;
        }

        public override Grenier CreateGrenier(string name)
        {
            Console.WriteLine($"\n{name} vient de créer un grenier !");
            Player1Grenier grenier = new Player1Grenier();
            Console.WriteLine($"PV : {grenier.Health}");
            return grenier;
        }

        public override Habitation CreateHabitation(string name)
        {
            Console.WriteLine($"\n{name} vient de créer une habitation !");
            Player1Habitation habitation = new Player1Habitation();
            Console.WriteLine($"PV : {habitation.Health}");
            return habitation;
        }

        public override Port CreatePort(string name)
        {
            Console.WriteLine($"\n{name} vient de créer un port !");
            Player1Port port = new Player1Port();
            Console.WriteLine($"PV : {port.Health}");
            return port;
        }

        public override PuitsDeStockage CreatePuit(string name)
        {
            Console.WriteLine($"\n{name} vient de créer un puit de stockage !");
            Player1PuitsDeStockage puit = new Player1PuitsDeStockage();
            Console.WriteLine($"PV : {puit.Health}");
            return puit;
        }
    }
}
