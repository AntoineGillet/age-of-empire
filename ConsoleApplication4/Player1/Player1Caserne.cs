﻿using ConsoleApplication4.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication4.Player1
{
    public class Player1Caserne : Caserne
    {
        public override Fantassin CreateFantassin()
        {
            Console.WriteLine("Un fantassin vient d'être créé !");
            return new Player1Fantassin();
        }
    }
}
