﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication4.Common
{
    public class ForumFactories
    {
        public static ForumFactory GetFactory(string Namespace)
        {
            Type TFactory = Assembly.GetExecutingAssembly().GetTypes().SingleOrDefault(t => t.Namespace == Namespace && t.IsSubclassOf(typeof(ForumFactory)));

            if (TFactory == null)
                throw new InvalidOperationException("There is no ForumFactory into the specified namespace!!");

            return (ForumFactory)Activator.CreateInstance(TFactory);
        }
    }
}
