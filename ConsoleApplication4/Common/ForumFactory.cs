﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication4.Common
{
    public abstract class ForumFactory
    {
        public abstract Caserne CreateCaserne(string name);
        public abstract PuitsDeStockage CreatePuit(string name);
        public abstract Grenier CreateGrenier(string name);
        public abstract Port CreatePort(string name);
        public abstract Habitation CreateHabitation(string name);
    }
}
